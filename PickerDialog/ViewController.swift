//
//  ViewController.swift
//  PickerDialog
//
//  Created by Htet Aung Lin on 18/05/2022.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    var selectedEventYear = "all"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func onClickButton(_ sender: Any) {
        var pickerData = [[String: String]]()
        pickerData.append(["2000": "2000"])
        pickerData.append(["2001": "2001"])
        pickerData.append(["2002": "2002"])
        pickerData.append(["2003": "2003"])
        pickerData.append(["2004": "2004"])
        let picker = PickerDialog()
        picker.backgroundColor = UIColor.black
        picker.show(title: "Choose Year", options: pickerData, selected: selectedEventYear
        ) {
            (value) -> Void in
            self.selectedEventYear = value
            self.button.setTitle(value, for: .normal)
            // TODO data refresh
        }
    }
    
}

